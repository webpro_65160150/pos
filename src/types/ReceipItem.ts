import type { Product } from "./Product";

type ReceiptItem = {
    id: number;
    name: string;
    price: number;
    unit: number;
    productId: number;
    producut?: Product;
}
export{ type ReceiptItem }